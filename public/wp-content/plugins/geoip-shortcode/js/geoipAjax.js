jQuery(document).ready( function ($) {
	if (typeof changeRegionalizedContent != 'function') {
		function changeRegionalizedContent(regionPref) {
			if (regionPref!='us') {//we default to us, so only need to change if non-us
				$('.geoip.us').addClass('hidden');
				$('.geoip.'+regionPref).removeClass('hidden');
				//cookie compliance message
				//this is now handled by TRION_CORE v1.0.1
/*				var cookieCountries = "fr de eu ad al at ba be bg by ch cy cz dk ee es fi gi gr hr hu ie is it kv li lt lu lv mc md mk me mt nl no pl pt ro ru rs se si sk sm ua va vc sc gb";
				if (cookieCountries.indexOf(regionPref)!=-1) {
					var hideCookieCompliance = TRION_CORE.readCookie("hide-cookie-compliance");
					//console.log(hideCookieCompliance);
					if (hideCookieCompliance){
						$(".cookie-compliance-container").css("display","none");
					} else {
						var htmlMarginTop=parseInt($("html").css("margin-top"))+103;
						htmlMarginTop="margin-top:"+htmlMarginTop+"px !important;";
						$("html").attr("style", htmlMarginTop);
						if (($("body").css("background-attachment")) != "fixed"){
							$("body").css("background-position","center 0");
						}
					}
				}
*/
			}
		}
	}

	//change page content based on country_code once it's available
	var interval=setInterval(function(){
		if (TRION_CORE.currentCountryCode!==null)
		{
			clearInterval(interval);
			changeRegionalizedContent(TRION_CORE.currentCountryCode);
		}
	},200);
});
